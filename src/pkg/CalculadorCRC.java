package pkg;

import javax.swing.JOptionPane;

public class CalculadorCRC {

    private String CRC, CRC2, MSJ, MSJ2, POLINOMIO;

    public CalculadorCRC() {
        CRC = "";
        CRC2 = "";
        MSJ = "";
        MSJ2 = "";
        POLINOMIO = "";
    }

    public String getCRC() {
        return CRC;
    }

    public void setCRC(String CRC) {
        this.CRC = CRC;

    }

    public String getCRC2() {
        return CRC2;
    }

    public void setCRC2(String CRC2) {
        this.CRC2 = CRC2;
    }

    public String getMSJ() {
        return MSJ;
    }

    public void setMSJ(String MSJ) {
        this.MSJ = eliminar_ceros(MSJ);
    }

    public String getMSJ2() {
        return MSJ2;
    }

    public void setMSJ2(String MSJ2) {
        this.MSJ2 = eliminar_ceros(MSJ2);
    }

    public String getPOLINOMIO() {
        return POLINOMIO;
    }

    public void setPOLINOMIO(String POLINOMIO) {
        this.POLINOMIO = eliminar_ceros(POLINOMIO);
    }

    public String error(String msj, String error) {
        String tmp = "";
        int j = 0;
        int medmsj=msj.length() / 2;
        int mederror=error.length() / 2;
        //JOptionPane.showMessageDialog(null, msj+" msj "+medmsj+"\n"+error+" Error: "+mederror);
        for (int i = 0; i < msj.length(); i++) {
            if (i >= (medmsj-mederror) && i < ( medmsj+ mederror)||(mederror==0&&i==medmsj)) {
                tmp += error.charAt(j);
                j++;
            } else {
                tmp += msj.charAt(i);
            }
        }
        return tmp;
    }

    public String redundancia(){
        String redundancia = "";
        for (int i = 0; i < (getPOLINOMIO().length() - 1); i++) {
            redundancia += "0";
        }
        return redundancia;
    }
    private char xor(char a, char b) {
        if (a == b) {
            return '0';
        } else {
            return '1';
        }
    }

    public String calcularCRC(String msjredu) {
        String divisor = "", residuo = "",poli=getPOLINOMIO();
        //System.out.println("Tam: " + msjredu.length());
        for (int i = 0; i < msjredu.length(); i++) {
            while (divisor.length() < poli.length() && i < msjredu.length()) {
                //System.out.println("i: " + i);
                divisor += msjredu.charAt(i);
                i++;
            }
            i--;//reduzco el ultimo no usado
            //System.out.println("Divisor: " + divisor);
            if (divisor.length() == poli.length()) {
                for (int j = 0; j < divisor.length(); j++) {
                    residuo += xor(divisor.charAt(j), poli.charAt(j));
                }
                divisor = eliminar_ceros(residuo);
            } else {
                residuo = divisor;
            }
            //System.out.println("Residuo: " + residuo);
            residuo = "";
        }
        while (divisor.length() < poli.length()-1) {
            divisor = "0" + divisor;
        }
        return divisor;
    }

    private String eliminar_ceros(String binario) {
        String tmp = "";
        boolean primer = false;
        for (int i = 0; i < binario.length(); i++) {
            char car = binario.charAt(i);
            if (car == '0' && !primer) {
                //ignoramos los ceros al comienzo
            } else {
                primer = true;
                tmp += car;
            }
        }
        return tmp;
    }

//CRC 1 agosto            
}
